<?php 

use CalDAVClient\Facade\CalDavClient;
use CalDAVClient\Facade\Requests\CalendarQueryFilter;
use voku\helper\HtmlDomParser;
use Sabre\VObject;
use FeedIo\Factory;
use FeedIo\Feed;


function retrieveCalContents($filepath, $limit)
{
	$today = date('Ymd');
	$two_months = date('Ymd', strtotime('+2 months'));

	$file = fopen($filepath, 'r');
	$items = [];
	$cal = [];
	$ical_header = "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-//hacksw/handcal//NONSGML v1.0//EN\n";
	$ical_header .= "BEGIN:VTIMEZONE
TZID:Europe/Rome
BEGIN:STANDARD
DTSTART:20201025T010000
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
TZNAME:CET
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:20210328T010000
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
TZNAME:CEST
END:DAYLIGHT
BEGIN:STANDARD
DTSTART:20211031T010000
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
TZNAME:CET
END:STANDARD
END:VTIMEZONE\n";
	$ical = $ical_header;

	while($row = fgetcsv($file)) {
		if ($row[0][0] == '#') {
			continue;
		}

		try {
			$events = [];
			switch ($row[1]) {
				case 'ical':
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/118.0');
					curl_setopt($ch, CURLOPT_URL, $row[2]);
					curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
						'Accept-Language: it-IT,en;q=0.5',
						'Accept-Encoding: gzip, deflate',
						'Connection: keep-alive',
						'Upgrade-Insecure-Requests: 1',
					));
					curl_setopt($ch, CURLOPT_FAILONERROR, true);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

					$contents = curl_exec($ch);

					if (curl_errno($ch)) {
						echo "Calendario " . $row[2] . " fallito! " . curl_error($ch) . "\n";
					} else {
						$events = VObject\Reader::read($contents, VObject\Reader::OPTION_FORGIVING)->getBaseComponents('VEVENT');
						foreach($events as $event) {
							if (empty($event->TZURL)) {
								$event->TZURL = $row[2];
								if (isset($row[4])) {
									$event->TZURL = $row[4];
								}
							}
						}
					}

					curl_close($ch);
					break;

				case 'caldav':
					try {
						$caldav = new CalDavClient($row[2]);
						$filter = new CalendarQueryFilter(true, true, new DateTime());
						$res  = $caldav->getEventsByQuery($row[2], $filter);
						foreach($res->getResponses() as $event) {
							$event = VObject\Reader::read($event->getVCard(), VObject\Reader::OPTION_FORGIVING)->getBaseComponents('VEVENT')[0];
							if (empty($event->TZURL)) {
								$event->TZURL = $row[2];
								if (isset($row[4])) {
									$event->TZURL = $row[4];
								}
							}
							$events[] = $event;
						}
					}
					catch(\Error $e) {
						echo "Errore fatale in lettura CalDav per " . $row[2] . " \n";
					}

					break;

				case 'meetup':
					try {
						$dom = HtmlDomParser::file_get_html($row[2]);
						$element = $dom->findOneOrFalse('#event-card-e-1');
						if ($element) {
							$date = $element->findOne('time')->innerText();
							$date = substr($date, 3);
							$date = str_replace(',', '', $date);
							$date = new \DateTime(str_replace(' CET', '', $date));

							$title = $element->findOne('span.ds-font-title-3')->innerText();
							$link = $element->getAttribute('href');

							$vcalendar = new VObject\Component\VCalendar();
							$events[] = $vcalendar->add('VEVENT',[
								'SUMMARY' => $title,
								'DESCRIPTION' => '[' . $link . '](' . $link . ')',
								'DTSTART' => $date,
								'DTEND' => $date->add(new DateInterval("PT1H")),
								'LOCATION' => $row[0],
								'TZURL' => $link,
								'UID' => md5($title . $date->format('Y-m-d H:i:s'))
							]);
						}
					}
					catch(\Error $e) {
						echo "Errore fatale in lettura Meetup per " . $row[2] . " \n";
						print_r($e);
					}

					break;
			}

			foreach($events as $event) {
				if ($event->dtstart < $today || $event->dtstart > $two_months) {
					continue;
				}
				
				if ( empty((string)$event->DESCRIPTION) ) {
					$event->DESCRIPTION = $row[0];
				}

				$description = str_replace( '__is_owner="true"', '', $event->DESCRIPTION );
				if ( is_array( $description ) ) {
					$description = implode( "\n", $description );
				}
				$description = Slimdown::render( $description );
				
				$event->DESCRIPTION = $description;
				$event->X_CALENDARIO = $row[0];
				$event->X_CITTA = $row[3];
				
				$event->SUMMARY = $row[0] . ': ' . $event->SUMMARY;
				$event->categories = $row[3];

				$cal = generateRegCal($event,$cal,$ical_header);
				
				$ical .= $event->serialize();

				$items[] = $event;
			}
			
		}
		catch(\Exception $e) {
			echo "Impossibile leggere il calendario di " . $row[0] . ": " . $e->getMessage() . "\n";
		}
	}
	
	$ical .= "END:VCALENDAR\n";
	file_put_contents('eventi/eventi.ics', $ical);
	ics2rss($items,'Nazionale','eventi/eventi.rss');
	fclose($file);

	saveRegCal($cal,$ical_header);

	usort($items, function($first, $second) {
		return (string) $first->dtstart <=> (string) $second->dtstart;
	});

	return array_slice(array_reverse($items), 0, $limit);
}

function formatCalPage($path, $data, $outfile)
{
	$sorted = [];

	foreach($data as $i) {
		$date = substr((string) $i->dtstart, 0, 8);

		if (!isset($sorted[$date])) {
			$sorted[$date] = [];
		}

		$sorted[$date][] = $i;
	}

	ksort($sorted);
		$data = $sorted;

	$html = '<div class="row">';

	$html .= '<div class="col-8" style="overflow-x: hidden">';

	foreach($data as $date => $events) {
		$formatted_date = join('/', [substr($date, -2), substr($date, -4, 2), substr($date, 0, 4)]);
		$html .= '<div class="intro"><h2 class="m-0">' . $formatted_date . '</h2></div><hr>';

		foreach($events as $event) {
			if (isset($event->tzurl)) {
				$html .= '<h4>' . $event->categories . ' - <a href="' . (string)$event->tzurl . '">' . $event->summary . '</a></h4>';
			} else {
				$html .= '<h4>' . $event->summary . '</h4>';
			}

			$description = (string)$event->description;

			if($description === (string)$event->X_CALENDARIO) {
				$dtstart = new DateTime($event->dtstart);
				$dtstart = $dtstart->format('Y-m-d H:i');
				$description = 'Il giorno ' . $dtstart . '<br>Non sono disponibili altre informazioni.';
			}

			if (!empty($event->location)) {
				if (strncmp($event->location, 'http', 4) == 0) {
					$location = '<br><br>Luogo: <a href="' . $event->location . '">' . $event->location . '</a>';
				}
				else {
					$location = trim($event->location);
					if (empty($location)) {
						$location = '<br><br>Calendario: ' . $event->X_CALENDARIO;
					} else {
						$location = '<br><br>Luogo: ' . $location . ' - ' . $event->X_CITTA;
					}
				}

				$html .= '<div>' . Slimdown::render( $description )  . $location . '</div><hr>';
			}
			else {
				$html .= '<div>' . Slimdown::render( $description ) . '</div><hr>';
			}
		}
	}

	$html .= '</div><div class="col-4">';

	file_put_contents($outfile, $html);

	$html = '<div class="intro">';
	$html .= '<h4 style="clear:both;" class="text-center">Elenco dei Calendari</h4>';

	$file = fopen($path, 'r');
	$html .= '<ul>';
	while($row = fgetcsv($file)) {
		if ($row[0][0] == '#') {
			continue;
		}
		$html .= '<li>';
		if ( isset( $row[4] ) ) {
			$html .= '<a href="' . $row[4] . '">' . $row[0] . '</a>';
			$html .= ' - <a href="' . $row[2] . '"><img src="/images/calendar-icon.png" style="margin-top: -10px;" alt="Feed ICS"></a></li>';
		} else {
			$html .= '<a href="' . $row[2] . '">' . $row[0] . '</a>';
		}

	}
	$html .= '</ul>';

	$html .= '
	<div class="modal" tabindex="-1" role="dialog" id="istruzioni">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Calendari</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Chiudi">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>
						Questa pagina aggrega automaticamente contenuti fino ai prossimi 2 mesi provenienti da calendari iCal, CalDAV o Meetup.com.<br>
						<h3>Per aggiungersi</h3>
						Per essere inseriti le comunità devono rientrare nelle libertà digitali con un interesse al mondo Open Source.<br>
						<i>Un calendario Google pubblico ha la versione ICS come anche i gruppi Mobilizon.</i>
					</p>
					<p>
						<b>Le segnalazioni manuali di eventi non sono gestite</b>.<br>
					</p>
					<p>
						Il codice di questo sito si trova su <a href="https://gitlab.com/ItalianLinuxSociety/planet/">GitLab con tutti i calendari</a>.<br>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" tabindex="-1" role="dialog" id="feed">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Feed per regione</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Chiudi">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					Per ricevere i feed via email puoi utilizzare il nostro <a href="https://forum.linux.it/t/definizione-della-categoria-eventi/265">Forum</a>, registrandoti e seguendo la categoria o il tag regionale che preferisci.<br>
					<ul>';
	$files = glob('eventi/calendari/*.{ics}', GLOB_BRACE);
	foreach($files as $file) {
			$html .= '<li><a href="webcal://planet.linux.it/' . $file . '"><img src="/images/calendar-icon.png" alt="Feed ICS"></a> <a href="https://planet.linux.it/' . str_replace('.ics','.rss',$file)  . '"><img src="/images/feed-icon.png" alt="Feed RSS"></a> - <a href="?regione=' . basename($file,'.ics') . '"><b>' . basename($file,'.ics') .'</b></a></li>'."\n";
	}

	$html .= '		</ul>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Chiudi</button>
				</div>
			</div>
		</div>
	</div>';

	$html .= '</div></div></div>';

	file_put_contents('eventi/modali.html', $html);
}

function generateRegCal($event,$cal,$ical_header){
	$regioni = json_decode(file_get_contents("eventi/regioni.json"), true);
	foreach ($regioni as $regione => $citta) {
		// Calendario regionale vuoto come predefinito
		file_put_contents('eventi/calendari/' . $regione . '.ics', $ical_header . "END:VCALENDAR\n");
		file_put_contents('eventi/calendari/' . $regione . '.rss', "");
		foreach ($citta as &$singolacitta) {
			if ((string)$event->X_CITTA === $singolacitta){
				$cal[$regione][] = $event;
				return $cal;
			}
		}
	}
	return $cal;
}

function saveRegCal($cal,$ical_header) {
	foreach ($cal as $regione => $events) {
		$ical = $ical_header;
		foreach ($events as &$event) {
			$ical .= $event->serialize();
		}
		$ical .= "END:VCALENDAR\n";
		file_put_contents('eventi/calendari/' . $regione . '.ics', $ical);
		ics2rss($events,$regione);
	}
}

function ics2rss($events,$name,$path = '') {
	$feed = new Feed();
	$feed->setTitle($name . ' Planet Linux Feed');
	$feed->addNS('atom', 'http://www.w3.org/2005/Atom');
	$feed->addNS('content', 'http://purl.org/rss/1.0/modules/content/');
	$feed->addNS('slash', 'http://purl.org/rss/1.0/modules/slash/');
	$feed->addNS('feedburner', 'http://rssnamespace.org/feedburner/ext/1.0');
	$feed->setLink('https://planet.linux.it/eventi/?regione=' . $name);
	$feed->setDescription('Feed generato da Planet.Linux.it');
	foreach ($events as &$event) {
		$item = $feed->newItem();
		$item->setLink($event->tzurl);
		$item->setTitle($event->summary);
		$item->setDescription($event->description);
		$item->setLastModified(new DateTime($event->dtstart));
		$item->setPublicId(urlencode($event->tzurl));
		if ( empty( $event->tzurl ) ) {
			$item->setPublicId(base64_encode($event->summary));
		}
		$feed->add($item);
	}
	echo "Salvataggio Feed RSS $name\n";
	$feedIo = Factory::create(
		['builder' => 'NullLogger'], // assuming you want feed-io to keep quiet
		['builder' => 'GuzzleClient', 'config' => ['verify' => false]] // ignore SSL errors
	)->getFeedIo();
	$xml = sanitizeRss($feedIo->toRss($feed));
	if ( empty($path) ) {
		file_put_contents('eventi/calendari/' . $name . '.rss', $xml);
	} else {
		file_put_contents($path, $xml);
	}
}
